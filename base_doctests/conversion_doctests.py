
# problem 2
def to_base3(num):
    """
    Convert an decimal integer into a string representation of its base3
    digits.

      >>> to_base3(10)
      '101'
      >>> to_base3(12)
      '110'
      >>> to_base3(6)
      '20'
    """
    finalnum = []

    finalnum.append(int(num / 3))

    if finalnum[0] > 2:
        finalnum[0] = (int(finalnum[0] / 3))
        numover3 = num / 3
        finalnum.append(int(numover3 % 3))
        finalnum.append(int(num % 3))

        for i in range(len(finalnum)):
            finalnum[i] = str(finalnum[i])
        return ''.join(finalnum)

    finalnum.append(num % 3)

    for i in range(len(finalnum)):
            finalnum[i] = str(finalnum[i])

    return ''.join(finalnum)
 
# problem 4
def to_base(num, base):
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 10) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
    """
    finalnum = []
    if base == 2:
        return bin(num).replace("0b","")
    finalnum.append(int(num / base))
    indicator = 0
    if finalnum[0] > base - 1:
        indicator = 1
    while finalnum[0] > base - 1:
        finalnum[0] = (int(finalnum[0] / base))
        numover3 = num / 3
        finalnum.append(int(numover3 % base))
        finalnum.append(int(num % base))
    
    if indicator == 0:
        finalnum.append(num % base)
    
    for i in range(len(finalnum)):
        finalnum[i] = str(finalnum[i])
    
    return ''.join(finalnum)    

if __name__=='__main__':
    import doctest
    doctest.testmod()
