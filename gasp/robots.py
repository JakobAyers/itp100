from gasp import *

scale = 10

begin_graphics()

a = 0
b = 0
ax = 0
ay = 0
bx = 0
by = 0

player_x = random_between(0, 63) * scale
player_y = random_between(0, 47) * scale
c = Circle((player_x, player_y), 5, filled=True)

def robot(r, robot_x, robot_y):
    robot_x = random_between(0, 63) * scale
    robot_y = random_between(0, 47) * scale
    r = Box((robot_x, robot_y), 10, 10, filled=True)

def movebot(r, robot_x, robot_y):
    coin = random_between(0,1)
    print ('1')
    if coin==1 and player_y != robot_y:
        if robot_y < player_y:
            robot_y += scale
        elif robot_y > player_y:
            robot_y -= scale
    
    elif coin == 0 and player_x != robot_x:
        if robot_x < player_x:
            robot_x += scale
        elif robot_x > player_x:
            robot_x -= scale
    move_to(r, (robot_x, robot_y))
    robot_x %= 640
    robot_y %= 480
    if robot_x == player_x and robot_y == player_y:
        finished = True
robot(a, ax, ay)
robot(b, bx, by)

instructions = Text("You are the circle. Run away from the Robot Square", (50, 400), size=20)

finished = False
moves = 0
counter = Text('0', (0, 0), size=48)

while not finished:
    key = update_when('key_pressed')

    if moves == 0:
        remove_from_screen(instructions)
        
    if key == 'up':
        player_y += scale
    elif key == 'down':
        player_y -= scale
    elif key == 'right':
        player_x += scale
    elif key == 'left':
        player_x -= scale
    move_to(c, (player_x, player_y))
    
    coin = random_between(0,1)
    
    print ('1')
    if coin==1 and player_y != robot_y:
        if robot_y < player_y:
            robot_y += scale
        elif robot_y > player_y:
            robot_y -= scale
    
    elif coin == 0 and player_x != robot_x:
        if robot_x < player_x:
            robot_x += scale
        elif robot_x > player_x:
            robot_x -= scale
    move_to(r, (robot_x, robot_y))
    robot_x %= 640
    robot_y %= 480
    if robot_x == player_x and robot_y == player_y:
        finished = True
 
    movebot(a, ax, ay)
    movebot(b, bx, by)
    
    remove_from_screen(counter)
    counter = Text(str(moves), (0, 0), size = 48)

    player_x = player_x % 640
    player_y %= 480
    moves += 1


end_text = Text(f"The game is OVER. You LOSE. You survived {moves} moves. Press 'q' to exit.", (50, 50), size=16)

while True:
    key = update_when('key_pressed')
    if key == 'q':
        break

end_graphics()  
