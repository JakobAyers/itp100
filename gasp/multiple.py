from gasp import *

scale = 20
x_range = 640 / scale
y_range = 480 / scale
robots = []

def r(r1):
    return random_between(0, r1)

for i in range(30):
    robots.append((r(x_range), r(y_range), r(255), r(255), r(255)))

begin_graphics()

while True:
    clear_screen()
    for i in range(len(robots)):
        Box((robots[i][0] * scale, robots[i][1] * scale), scale, scale, color = (robots[i][2], robots[i][3], robots[i][4]), filled=True)
    key = update_when('key_pressed'):
    if key == 'q': 
        break

end_graphics()
