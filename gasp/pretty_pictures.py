from gasp import *

begin_graphics()


def Face(x, y):
    Circle((x + 300, y + 200), 50)
    Circle((x + 285, y + 210), 5)
    Circle((x + 315, y + 210), 5)
    Line((x + 270, y + 220), (x + 290, y + 217))
    Line((x + 330, y + 220), (x + 310, y + 217))
    Line((x + 300, y + 210), (x + 290, y + 190))
    Line((x + 290, y + 190), (x + 310, y + 190))
    Arc((x + 300, y + 200), 30, 225, 315)

    Circle((x + 282, y + 210), 1)
    Circle((x + 318, y + 210), 1)

Face(0, 0)
Face(150, 0)

update_when('key_pressed')

end_graphics()
