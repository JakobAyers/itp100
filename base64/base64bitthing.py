def base64encode(num):
    """
    >>> base64encode(b'\\x49\\x33\\x8F')
    'STOP'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    b1, b2, b3 = num[0], num[1], num[2]
    index1 = b1 >> 2
    index2 = (b1 & 3) << 4 | b2 >> 4
    index3 = (b2 & 15) << 2 | b3 >> 6
    index4 = b3 & 63
    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'

if __name__=='__main__':
    import doctest
    doctest.testmod()

