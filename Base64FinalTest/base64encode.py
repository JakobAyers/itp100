#!/usr/bin/env python3
import os
import sys

from base64_tools import base64encode


def main():
    try:
        infile = open(sys.argv[1], 'rb')
    except IndexError:
        print('Requires file to encode as argument.')
    except FileNotFoundError:
        print(f'{sys.argv[1]} file not found.')

    outfile = open(sys.argv[1].replace('.', '__') + '.b64txt', 'w')
    outfile.write(base64encode(infile))

    infile.close()
    outfile.close()


if __name__ == '__main__':
    main()
