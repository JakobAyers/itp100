
def only_evens(nums):
    """
      >>> only_evens([3, 8, 5, 4, 12, 7, 2])
      [8, 4, 12, 2]
      >>> my_nums = [4, 7, 19, 22, 42]
      >>> only_evens(my_nums)
      [4, 22, 42]
      >>> my_nums
      [4, 7, 19, 22, 42]
    """
    final=[]
    for i in nums:
        if i % 2 == 0:
            final.append(i)
    return final
    
##############################3
def num_even_digits(n):
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    final = 0
    for i in str(n):
        if int(i) % 2 == 0:
            final += 1
    return final 

###################################3

def sum_of_squares_of_digits(n):
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_of_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    e = 0
    for i in str(n):
        e += int(i) ** 2
    return e
###############################################
def lots_of_letters(word):
    """
      >>> lots_of_letters('lidia')
      'liidddiiiiaaaaa'
      >>> lots_of_letters('python')
      'pyyttthhhhooooonnnnnn'
      >>> lots_of_letters('')
      ''
      >>> lots_of_letters('1')
      '1'
    """
    final = ''
    for l in range(len(word)):
        e = l + 1
        final += word[l] * e
    return final
##############################################################
def gcf(m, n):
    """
      >>> gcf(10, 25)
      5
      >>> gcf(8, 12)
      4
      >>> gcf(5, 12)
      1
      >>> gcf(24, 12)
      12
    """ 
    for e in range(1, 100):
        i = 13 - e

        if m % i  == 0 and n % i == 0: 
            return i
##############################################################
def is_prime(n):
    """
      >>> is_prime(7)
      'yes'
      >>> is_prime(6)
      'no'
      >>> is_prime (13)
      'yes'
    """
    for i in range(2, int(n/2)):
        if n % i == 0:
            return 'no'
    return 'yes'


if __name__ == '__main__':
    import doctest
    doctest.testmod()
