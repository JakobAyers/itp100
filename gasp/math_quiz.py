from gasp import *
import random

correct = 0

for i in range (0, 10):
    num1 = random.randint(1, 15)
    num2 = random.randint(1, 15)
    e = input(f'What is {num1} times {num2}? ')
    answer = num1 * num2
    if str(answer) == str(e):
        print("That's right-- well done.")
        correct += 1
    else:
        print("No, I'm afraid the answer is " + str(answer) + '.')

print (f'You got {correct} correct!')
