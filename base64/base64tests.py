nums64 = ''
for i in range(26):
    nums64 += chr(65 + i)
for i in range(26):
    nums64 += chr(97 + i)
for i in range(10):
    nums64 += str(i)
nums64 += ('+')
nums64 += ("/")

nums = [0,1,2,3,4,5,6,7,8,9,10]
nums += nums64
def to_base(num, base):
    """
      >>> to_base(64, 64)
      'BA'
      >>> to_base(128, 64)
      'CA'
      >>> to_base(127, 64)
      'B/'
      >>> to_base(61700, 64)
      'PEE'
    """
    numstr = ''
    if base == 64:
        if num == 0: 
            return('A')

        while num: 
            numstr = nums64[num % base] + numstr
            num //= base
        return numstr
    numstr = ''
    
    if num == 0:
        return '0'
    while num:
        numstr = nums[num%base] + numstr
        num //= base
    return numstr

if __name__ == '__main__':
    import doctest
    doctest.testmod()
