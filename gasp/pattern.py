from gasp import *

begin_graphics()
H = 480
W = 640

for i in range(0, 240, 10):
    Line((i, 0), (0, 240-i))
    Line((i, 480), (0, 240+i))
    Line((W-i, 0), (W, 240-i))
    Line((W-i, 480), (W, 240+i))


update_when('key_pressed')
end_graphics()
