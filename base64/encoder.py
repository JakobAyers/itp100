def encode(num):
    num64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYabcdefghijklmnopqrstuvwxyz0123456789+/'
    numstr = ''
    if num == 0:
        return('A')
    
    while num:
        numsrtr = num64[num % 64] + numstr
        num //= 64
    return numstr

def to64(text):
    final = ''
    for i in text:
        final += encode(ord(i))
    return final
print (to64('yeehaw'))



if __name__ == '__main__':
    import doctest
    doctest.testmod()
